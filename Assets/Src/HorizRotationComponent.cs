﻿
using UnityEngine;

namespace Turret
{
    //*****************************************************************************//
    // логика вращения объекта
    //*****************************************************************************//
    public class HorizRotationComponent : MonoBehaviour
    {
        
        [SerializeField] GameObject RotatedObject;  // ссылка на вращаемый объект       
        [SerializeField] float SpeedRotate; // скорость вращения
        // [SerializeField] RotationState _currentRotationState; // для отладки


        private RotationState _currentRotationState;


        public void SetRotateState(RotationState state)
        {
            _currentRotationState = state;
        }


        private void Start()
        {
            _currentRotationState = RotationState.NoRotation;
        }



        private void Update()
        {
            if (_currentRotationState == RotationState.LeftRotation)
                RotationLeft();
            else if (_currentRotationState == RotationState.RightRotation)
                RotationRight();
        }



        private void RotationLeft()
        {
            RotatedObject.transform.Rotate(0, - SpeedRotate * Time.deltaTime, 0);
        }

        private void RotationRight()
        {
            RotatedObject.transform.Rotate(0, SpeedRotate * Time.deltaTime, 0);
        }



        
    }
}


