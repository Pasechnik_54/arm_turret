﻿
namespace Turret {

    public enum RotationState {
        NoRotation,
        LeftRotation,
        RightRotation
    }
}
