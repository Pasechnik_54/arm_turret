﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//*****************************************************************************//
// логика кнопок, управляющих вращением турели
//*****************************************************************************//

namespace Turret 
{
    public class PlayerRotationController : MonoBehaviour {

        [SerializeField] GameObject RotatedTurret; 
        [SerializeField] GameObject LeftButton;
        [SerializeField] GameObject RightButton;


        // ссылка на компонент вращения турели
        private HorizRotationComponent _turretMovement;
        // состояния кнопок
        private MyButtonEventSource _leftButton;
        private MyButtonEventSource _rightButton;

        // Start is called before the first frame update
        void Start() 
        {
            _turretMovement = RotatedTurret.GetComponent<HorizRotationComponent>();
            _leftButton = LeftButton.GetComponent<MyButtonEventSource>();
            _rightButton = RightButton.GetComponent<MyButtonEventSource>();
        }

        // Update is called once per frame
        void Update() 
        {

            if ( _leftButton.ButtonPressed && _rightButton.ButtonPressed)
            {
                Debug.Log("Pressed all buttons");
                return;
            }
            else if ( !_leftButton.ButtonPressed && !_rightButton.ButtonPressed )
            {
                //Debug.Log("Don't pressed buttons");
                _turretMovement.SetRotateState(RotationState.NoRotation);
            }

            else if (_leftButton.ButtonPressed)
            {
                //Debug.Log("Pressed left button");
                _turretMovement.SetRotateState(RotationState.LeftRotation);
            }
            else if (_rightButton.ButtonPressed)
            {
                //Debug.Log("Pressed right button");
                _turretMovement.SetRotateState(RotationState.RightRotation);
            }

        }
    }
}
