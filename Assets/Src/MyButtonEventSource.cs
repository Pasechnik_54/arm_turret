﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//*****************************************************************************//
// хранит данные о состоянии кнопки 
//*****************************************************************************//
namespace Turret 
{
    public class MyButtonEventSource : MonoBehaviour {

        public bool ButtonPressed;
        // Start is called before the first frame update
        void Start() {
            ButtonPressed = false;
        }

        // Update is called once per frame
        void Update() {

        }
        

        public void setButtonPressed()
        {
            //Debug.Log("mouse down");
            ButtonPressed = true;
        }

        public void clearButtonPressed() 
        {
            //Debug.Log("mouse up");
            ButtonPressed = false;
        }

       
    }

}
